import string

def getWords(tweet):
    parts = tweet.split(' ')
    words = []
    for word in parts:
        if word.find('@') and word.find('http'):
            word = word.translate(None, string.punctuation)
            if word:
                words.append(word.lower())
    return words

def calcProbability(word, words, positiveP, K):
    if not word in words:
        return 1
    return float(positiveP[word]  + K) / (float(words[word] + K*2) * 0.5)