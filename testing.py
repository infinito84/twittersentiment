import csv, sys, util

words = {}
positiveP = {}

def readValues():
    with open('sampling.csv', 'rb') as f:
        reader = csv.reader(f)
        for row in reader:
            positiveP[row[0]] = int(row[1])
            words[row[0]] = int(row[2])

def test(K, per):
    testResult = {'truePositive' : 0, 'falsePositive': 0, 'trueNegative' : 0, 
                'falseNegative' :0, 'positive' : 0, 'negative' : 0}

    with open('test/test.csv', 'rb') as f:
        reader = csv.reader(f)
        for row in reader:
            result = row[0]
            probability = 1
            for word in util.getWords(row[5]):
                probability = probability * util.calcProbability(word, words, positiveP, K)
            
            if result == '4':
                if probability >= per:
               	    testResult['truePositive'] = testResult['truePositive'] + 1
                else:
                    testResult['falsePositive'] = testResult['falsePositive'] + 1
                testResult['positive'] = testResult['positive'] + 1
            
            if result == '0':
                if probability < per:
                    testResult['trueNegative'] = testResult['trueNegative'] + 1
                else:
                    testResult['falseNegative'] = testResult['falseNegative'] + 1
                testResult['negative'] = testResult['negative'] + 1

    positiveResult = float(testResult['truePositive'])/float(testResult['positive'])
    negativeResult = float(testResult['trueNegative'])/float(testResult['negative'])
    total = (positiveResult + negativeResult)*100/2
    return total, testResult