import csv, sys, util

def train():
    words = {}
    positiveP = {}

    with open('test/training.csv', 'rb') as f:
        reader = csv.reader(f)
        count = 0
        positive = 0
        for row in reader:
            result = row[0]
            for word in util.getWords(row[5]):
                if not word in words:
                    words[word] = 0
                    positiveP[word] = 0
                words[word] = words[word] + 1
                if result == '4':
                    positiveP[word] = positiveP[word] + 1
            
            if result == '4':
                positive = positive + 1

            count = count + 1
            if count % 1000 == 0:
                sys.stdout.write('\rTraining Data: '+str(float(count) / 16000) + '%')
                sys.stdout.flush()

        print "\nBag of words: ", len(words.keys())
        print "P(sentiment=positive) = ", float(positive) / float(count);

        with open('sampling.csv', 'wb') as csvfile:
            sampling = csv.writer(csvfile, delimiter=',',
                                    quotechar='"', quoting=csv.QUOTE_ALL)
            for key in words.keys():
            	sampling.writerow([key, positiveP[key], words[key]])