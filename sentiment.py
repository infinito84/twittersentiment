import sys

if __name__ == "__main__":
    last = sys.argv.pop()
    
    if last == 'train':
        import training
        training.train()
    
    elif last == 'test':
        import testing
        testing.readValues()
        testing.test(1, 0.5)

    else:
        import testing
        testing.readValues()
        maxValue = 0
        maxPer = 0
        maxItem = None
        for i in range(101):
            per = i * 0.01
            value, item = testing.test(1, per)
            if value > maxValue:
                maxValue = value
                maxPer = per
                maxItem = item

        print "Best result: "
        print str(maxValue) + "%"
        print "Positive from : " + str(maxPer) + "%"
        print "Values: ", maxItem